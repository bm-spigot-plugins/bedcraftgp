package me.Zeuss.bedcraftgp;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	  public static List<String> itemids;
	  public static List<String> itemids3;
	  public static List<String> click;
	  public static String remtext;
	  public static String qtext;
	  public static String clicktext;
	  public static String minimumstone;
	  private static Main pl;
	  
	  
	  @Override
	  public void onEnable() {
		  pl = this;
		  Plugin plugin = Bukkit.getPluginManager().getPlugin("GriefPrevention");
		  if(plugin == null){
			  Bukkit.getConsoleSender().sendMessage("�cN�o foi poss�vel localizar o plugin: GriefPrevention.");
			  Bukkit.getPluginManager().disablePlugin(this);
			  return;
		  }
		  saveDefaultConfig();
		  itemids = getConfig().getStringList("RightClickBlockInClaim");
		  itemids3 = getConfig().getStringList("ClickAroundClaim");
		  click = getConfig().getStringList("AirclickWorld");
		  remtext = getConfig().getString("ClaimClickMessage");
		  qtext = getConfig().getString("QuarryMessage");
		  clicktext = getConfig().getString("AreaClickMessage");
		  minimumstone = getConfig().getString("PreventMinimumStoneGrief");
		    
		  getServer().getPluginManager().registerEvents(new BlockPlace(), this);
		  getServer().getPluginManager().registerEvents(new PlayerInteract(), this);
	  }
	  
	  public static Main getPlugin(){
		  return pl;
	  }
	  
	  
	}
