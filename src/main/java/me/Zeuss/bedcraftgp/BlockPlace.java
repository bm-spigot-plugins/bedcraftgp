package me.Zeuss.bedcraftgp;

import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlace implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.HIGH)
	public void event(BlockPlaceEvent e){
		Player player = e.getPlayer();
	    Block block = e.getBlock();
	    if(Main.getPlugin().getConfig().getBoolean("Quarry.Enable")){
	    	if (block.getTypeId() == Main.getPlugin().getConfig().getInt("Quarry.Id")){
	    		int y = block.getY();
	    	    int x = block.getX();
	    	    int z = block.getZ();
	    	    World world = block.getWorld();
	    	    Block c9 = world.getBlockAt(x, y, z);
	    	    Block c1 = world.getBlockAt(x + 11, y, z);
	    	    Block c2 = world.getBlockAt(x - 11, y, z);
	    	    Block c3 = world.getBlockAt(x, y, z + 11);
	    	    Block c4 = world.getBlockAt(x, y, z - 11);
	    	    Block c5 = world.getBlockAt(x + 11, y, z + 11);
	    	    Block c6 = world.getBlockAt(x + 11, y, z - 11);
	    	    Block c7 = world.getBlockAt(x - 11, y, z + 11);
	    	    Block c8 = world.getBlockAt(x - 11, y, z - 11);
	    	    boolean check = false;
	    	    if (!checkBlock(player, c1)) {
	    	        check = true;
	    	    }
	    	    if (!checkBlock(player, c2)) {
	    	        check = true;
	    	    }
	    	    if (!checkBlock(player, c3)) {
	    	        check = true;
	    	    }
	    	    if (!checkBlock(player, c4)) {
	    	        check = true;
	    	    }
	    	    if (!checkBlock(player, c5)) {
	    	        check = true;
	    	    }
	    	    if (!checkBlock(player, c6)) {
	    	        check = true;
	    	    }
	    	    if (!checkBlock(player, c7)) {
	    	        check = true;
	    	    }
	    	    if (!checkBlock(player, c8)) {
	    	        check = true;
	    	    }
	    	    if (!checkBlock(player, c9)) {
	    	        check = true;
	    	    }
	    	    if (check){
	    	    	e.setCancelled(true);
	    	        player.sendMessage(Main.qtext);
	    	    }
		    }
	    }
	}
	
	private boolean checkBlock(Player player, Block block){
		String c = GriefPrevention.instance.allowBreak(player, block, block.getLocation());
		if (c != null) {
			return false;
		}
		return true;
	}
	
}
