package me.Zeuss.bedcraftgp;

import me.ryanhamshire.GriefPrevention.GriefPrevention;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteract implements Listener {

    @SuppressWarnings("deprecation")
    @EventHandler(priority = EventPriority.HIGH)
    public void event(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (Main.itemids.contains(String.valueOf(player.getItemInHand().getTypeId()))) {
                Block block = e.getClickedBlock();
                if (!checkBlock(player, block)) {
                    player.sendMessage(Main.remtext);
                    e.setCancelled(true);
                }
            }
        }
        if (Main.itemids3.contains(String.valueOf(player.getItemInHand().getTypeId())) || Main.itemids3.contains(String.valueOf(player.getItemInHand().getTypeId()) + ":*") || Main.itemids3.contains(String.valueOf(player.getItemInHand().getTypeId()) + ":" + player.getItemInHand().getDurability())) {
            Location loc = player.getLocation();
            int y = loc.getBlockY();
            int x = loc.getBlockX();
            int z = loc.getBlockZ();
            World world = loc.getWorld();
            Block block = world.getBlockAt(x, y, z);
            if (!check(player, block)) {
                e.setCancelled(true);
                player.sendMessage(Main.clicktext);
            }
        } else if ((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() != Action.RIGHT_CLICK_BLOCK)) {
            if (Main.click.contains(String.valueOf(player.getItemInHand().getTypeId())) || Main.click.contains(String.valueOf(player.getItemInHand().getTypeId()) + ":*") || Main.click.contains(String.valueOf(player.getItemInHand().getTypeId()) + ":" + player.getItemInHand().getDurability())) {
                if (!check(player, player.getLocation().getBlock())) {
					e.setCancelled(true);
				}
            }
        }
    }

//	@SuppressWarnings("deprecation")
//	@EventHandler(priority=EventPriority.HIGH)
//	public void event(PlayerInteractEvent e){
//		Player player = e.getPlayer();
//		if (e.getAction() == Action.RIGHT_CLICK_BLOCK){
//			if (Main.itemids.contains(String.valueOf(player.getItemInHand().getTypeId()))){
//				Block block = e.getClickedBlock();
//				if (!checkBlock(player, block)){
//					player.sendMessage(Main.remtext);
//					e.setCancelled(true);
//				}
//			}
//		}
//		else if ((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() != Action.RIGHT_CLICK_BLOCK)) {}
//		if (Main.itemids3.contains(String.valueOf(player.getItemInHand().getTypeId()))){
//			Location block = player.getLocation();
//			int y = block.getBlockY();
//			int x = block.getBlockX();
//			int z = block.getBlockZ();
//			World world = block.getWorld();
//			Block c9 = world.getBlockAt(x, y, z);
//			Block c1 = world.getBlockAt(x + 11, y, z);
//			Block c2 = world.getBlockAt(x - 11, y, z);
//			Block c3 = world.getBlockAt(x, y, z + 11);
//			Block c4 = world.getBlockAt(x, y, z - 11);
//			Block c5 = world.getBlockAt(x + 11, y, z + 11);
//			Block c6 = world.getBlockAt(x + 11, y, z - 11);
//			Block c7 = world.getBlockAt(x - 11, y, z + 11);
//			Block c8 = world.getBlockAt(x - 11, y, z - 11);
//			boolean check = false;
//			if (!checkBlock(player, c1)) {
//				check = true;
//			}
//			if (!checkBlock(player, c2)) {
//				check = true;
//			}
//			if (!checkBlock(player, c3)) {
//				check = true;
//			}
//			if (!checkBlock(player, c4)) {
//				check = true;
//			}
//			if (!checkBlock(player, c5)) {
//				check = true;
//			}
//			if (!checkBlock(player, c6)) {
//				check = true;
//			}
//			if (!checkBlock(player, c7)) {
//				check = true;
//			}
//			if (!checkBlock(player, c8)) {
//				check = true;
//			}
//			if (!checkBlock(player, c9)) {
//				check = true;
//			}
//			if (check){
//				e.setCancelled(true);
//				player.sendMessage(Main.clicktext);
//			}
//		}
//		else if (e.getAction() == Action.RIGHT_CLICK_AIR){
//			if (Main.click.contains(String.valueOf(player.getItemInHand().getTypeId()))) {
//				e.setCancelled(true);
//			}
//		}
//	}

    private boolean check(Player player, Block block) {
        Location loc = player.getLocation();
        int y = loc.getBlockY();
        int x = loc.getBlockX();
        int z = loc.getBlockZ();
        World world = block.getWorld();
        for (int i = 0; i < 9; i++) {
            Block b = block;
            switch (i) {
                case 1:
                    b = world.getBlockAt(x + 11, y, z);
                    break;
                case 2:
                    b = world.getBlockAt(x - 11, y, z);
                    break;
                case 3:
                    b = world.getBlockAt(x, y, z + 11);
                    break;
                case 4:
                    b = world.getBlockAt(x, y, z - 11);
                    break;
                case 5:
                    b = world.getBlockAt(x + 11, y, z + 11);
                    break;
                case 6:
                    b = world.getBlockAt(x + 11, y, z - 11);
                    break;
                case 7:
                    b = world.getBlockAt(x - 11, y, z + 11);
                    break;
                case 8:
                    b = world.getBlockAt(x - 11, y, z - 11);
                    break;
            }
            String c = GriefPrevention.instance.allowBreak(player, b, b.getLocation());
            if (c != null) {
                return false;
            }
        }
        return true;
    }

    private boolean checkBlock(Player player, Block block) {
        String c = GriefPrevention.instance.allowBreak(player, block, block.getLocation());
        if (c != null) {
            return false;
        }
        return true;
    }

}
